import React from 'react'
import { merge } from 'lodash'
import * as Cookies from "js-cookie";
import RtmClient from '../../rtm-client'
import { AGORA_APP_ID } from "../../agora.config";
/**
 * @prop appId uid
 * @prop transcode attendeeMode videoProfile channel baseMode
 */



class ChatBox extends React.Component {

  state = {
    devices: [],
    interval: undefined,
    enabledScreenShare: false,
    message: '',
    channelName: Cookies.get("channel"),
    rtm : new RtmClient()
  }

  constructor(props) {
    super(props)
    //const rtm = new RtmClient()
    this.sendmessage = this.sendmessage.bind(this);
  }

componentWillMount () {

  //rtm =  new RtmClient()
  this.state.rtm.init(AGORA_APP_ID)
  //window.rtm = rtm
  this.state.rtm.login(this.state.userName, '').then(() => {
    console.log('login')
    this.state.rtm._logined = true
    this.state.rtm.joinChannel(this.state.channel).then(() => {
      // $('#log').append(view)
      rtm.channels[this.state.channel].joined = true
    }).catch((err) => {
      // Toast.error('Join channel failed, please open console see more details.')
      console.error(err)
    })
    // Toast.notice('Login: ' + params.accountName, ' token: ', params.token)
  }).catch((err) => {
    console.log(err)
  })

  this.state.rtm.on('ConnectionStateChanged', (newState, reason) => {
    console.log('reason', reason)
    const view = $('<div/>', {
      text: ['newState: ' + newState, ', reason: ', reason].join('')
    })
    $('#log').append(view)
    if (newState === 'ABORTED') {
      if (reason === 'REMOTE_LOGIN') {
        // Toast.error('You have already been kicked off!')
        // $('#accountName').text('Agora Chatroom')
  
        rtm.clearState()
        // $('#dialogue-list')[0].innerHTML = ''
        // $('#chat-message')[0].innerHTML = ''
      }
    }
  })
  
  this.state.rtm.on('MemberJoined', ({ channelName, args }) => {
    const memberId = args[0]
    debugger;
    this.setState({message: memberId})
  })
  
  this.state.rtm.on('MemberLeft', ({ channelName, args }) => {
    const memberId = args[0]
    console.log('channel ', channelName, ' member: ', memberId, ' joined')
    const view = $('<div/>', {
      text: ['event: MemberLeft ', ', channel: ', channelName, ', memberId: ', memberId].join('')
    })
    $('#log').append(view)
  })
  
  this.state.rtm.on('ChannelMessage', ({ channelName, args }) => {
    const [message, memberId] = args
    console.log('channel ', channelName, ', messsage: ', message.text, ', memberId: ', memberId)
    const view = $('<div/>', {
      text: ['event: ChannelMessage ', 'channel: ', channelName, ', message: ', message.text, ', memberId: ', memberId].join('')
    })
    $('#log').append(view)
  })
}

sendmessage() {
  console.log('text')
  this.state.rtm.sendChannelMessage('appmessage', Cookies.get("channel")).then(() => {
    this.setState({message: 'text'})
  }).catch((err) => {
    // Toast.error('Send message to channel ' + params.channelName + ' failed, please open console see more details.')
    console.error(err)
  })
}

  render() {
    const {message} = this.state;
    return (
      <div style={{position:'absolute', top:'0'}}>
          <div className="col" style={{minWidth: '433px', maxWidth: '443px'}}>
            <div className="card" style={{marginTop: '0px', 'marginBottom': '0px'}}>
              <div className="row card-content" style={{marginBottom: '0px', marginTop: '10px'}}>

                        <div className="input-field channel-padding">
                          <label for="channelMessage" className="active">Channel Message</label>
                          <input type="text" placeholder="channel message" name="channelMessage" />
                            <button className="btn btn-raised btn-primary waves-effect waves-light custom-btn-pin" onClick={this.sendmessage.bind(this)} id="send_channel_message">SEND</button>
              </div>
                          {/* <div className="input-field">
                            <label for="peerId" className="active">Peer Id</label>
                            <input type="text" placeholder="peer name" name="peerId" />
              </div>
                            <div className="input-field channel-padding">
                              <label for="peerMessage" className="active">Peer Message</label>
                              <input type="text" placeholder="peer name" name="peerMessage" />
                                <button className="btn btn-raised btn-primary waves-effect waves-light custom-btn-pin" id="send_peer_message">SEND</button>
              </div>
                              <div className="input-field channel-padding">
                                <label for="memberId" className="active">query member</label>
                                <input type="text" placeholder="query member online status" name="memberId" />
                                  <button className="btn btn-raised btn-primary waves-effect waves-light custom-btn-pin" id="query_peer">QUERY</button>
              </div> */}
                              </div>
                            </div>
                          </div>
                        <div className="col s7 log-container" id="log">
                        {message}
                        </div>
                      </div>)
                }
              }
              
export default ChatBox